package com.chat.proto.thread.view.controller

import android.support.v7.app.ActionBarDrawerToggle
import android.util.Log
import android.view.View
import com.chat.proto.chat.view.controller.ChatController
import com.chat.proto.model.Chat
import com.chat.proto.model.ChatEntity
import com.chat.proto.model.Message
import com.chat.proto.model.MessageEntity

/**
 * Created by serg on 24/01/2017.
 */
class ThreadController(val rootId: Long, val text : String, chat: Chat, toggle: ActionBarDrawerToggle?) : ChatController(chat, toggle) {

    constructor() : this(0L, "", ChatEntity(), null) {
    }

    override fun title(): String? = text

    override fun subscribeToMessages() {
        if (subscription?.isUnsubscribed ?: false) {
            subscription!!.unsubscribe()
        }

        subscription = dataStore.select(Message::class)
                .where(MessageEntity.CHAT_ID.eq(chat.id))
                .and(MessageEntity.ID.eq(rootId).or(MessageEntity.REPLY_ID.eq(rootId)))
                .orderBy(MessageEntity.ID.desc())
                .limit(BUCKET * pagesCount)
                .get()
                .toSelfObservable()
                .subscribe(
                        { messages ->
                            run {
                                addMessages(messages.toList())
                            }
                        },
                        { err ->
                            run {
                                Log.e("", "error", err)
                                list.visibility = View.GONE
                                emptyTextTV.text = "Error happened"
                                emptyTextTV.visibility = View.VISIBLE
                            }
                        }
                )
    }


}