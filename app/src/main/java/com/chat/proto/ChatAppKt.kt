package com.chat.proto

import android.app.Application
import com.airbnb.rxgroups.ObservableManager
import com.chat.proto.core.di.AppComponentKt
import com.chat.proto.core.di.AppModuleKt
import com.chat.proto.core.di.DaggerAppComponentKt
import com.facebook.stetho.Stetho
import timber.log.Timber
import timber.log.Timber.DebugTree




class ChatAppKt : Application() {

    companion object {
        @JvmStatic lateinit var graph: AppComponentKt
    }

    private val observableManager = ObservableManager()


    override fun onCreate() {
        super.onCreate()

        Stetho.initializeWithDefaults(this);
        Timber.plant(DebugTree())

        graph = DaggerAppComponentKt.builder()
                .appModuleKt(AppModuleKt(this))
                .build()
        graph.inject(this)
    }

    internal fun observableManager(): ObservableManager {
        return observableManager
    }

}