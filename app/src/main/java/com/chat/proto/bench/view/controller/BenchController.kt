package com.chat.proto.bench.view.controller

import android.support.v7.app.ActionBarDrawerToggle
import android.view.View
import android.view.View.VISIBLE
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import com.chat.proto.R
import com.chat.proto.core.utility.EntityCreators
import com.chat.proto.core.view.BaseInnerController
import com.chat.proto.model.ChatType
import com.chat.proto.model.Message
import com.chat.proto.model.User
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.util.*

/**
 * Created by serg on 21/01/2017.
 */
class BenchController(toggle: ActionBarDrawerToggle?) : BaseInnerController(toggle) {

    constructor() : this(null) {
    }

    lateinit var syncResult: TextView
    lateinit var syncStatus: TextView
    lateinit var threadResult: TextView
    lateinit var threadStatus: TextView
    lateinit var runInThread: Button
    lateinit var asyncPB: ProgressBar
    lateinit var runSync: Button
    lateinit var syncPB: ProgressBar

    lateinit var usersList: MutableList<Message>

    override fun title(): String? = "Benchmark"

    override fun layoutId(): Int = R.layout.benchmark_layout

    override fun onViewBound(view: View) {
        super.onViewBound(view)

        syncResult = view.findViewById(R.id.sync_result) as TextView
        syncStatus = view.findViewById(R.id.sync_status) as TextView
        threadResult = view.findViewById(R.id.thread_result) as TextView
        threadStatus = view.findViewById(R.id.thread_status) as TextView
        runInThread = view.findViewById(R.id.WorkInThread) as Button
        runSync = view.findViewById(R.id.WorkSync) as Button
        syncPB = view.findViewById(R.id.sync_pb) as ProgressBar
        asyncPB = view.findViewById(R.id.async_pb) as ProgressBar

        val country = EntityCreators.createCountry(1, "+7", "Russia")
        val chat1 = EntityCreators.createChat(3, "Android Team", ChatType.CHAT, Date(), null, mutableSetOf<User>())
        val user = EntityCreators.createUser(3L, "Bron", "1221112244", country, Date(), null)
        usersList = (0..10000L)
                .map { EntityCreators.createMessage(it, "Android Team message " + it, chat1, Date(), user) }
                .toMutableList()

        runSync.setOnClickListener {
            runSyncroniuosCalculation(usersList)
        }

        runInThread.setOnClickListener {
            runAsyncCalculation(usersList)
        }

    }

    fun runAsyncCalculation(usersList: MutableList<Message>) {
        threadStatus.text = "started"
        asyncPB.visibility = VISIBLE
        var start = System.nanoTime()
        Observable.just(usersList)
                .subscribeOn(Schedulers.computation())
                .flatMapIterable {
                    list -> list
                }
                .filter { msg -> msg.id != 100000000L }
                .count()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { size ->
                            run {
                                var end = System.nanoTime()
                                var duration = (end - start)/1000000
                                Timber.d("thread finished size:${size} duration:${duration}")
                                threadStatus.text = "finished"
//                                asyncPB.visibility = GONE
                            }
                        },
                        { err ->
                            run {
                                Timber.e("error", err)
                                threadStatus.text = "error"
                            }
                        }
                )
    }

    private fun runSyncroniuosCalculation(usersList: MutableList<Message>) {
        syncStatus.text = "started"
        syncPB.visibility = VISIBLE
        var start = System.nanoTime()
        Observable.just(usersList)
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMapIterable {
                    list -> list
                }
                .filter { msg -> msg.id != 100000000L }
                .count()
                .subscribe(
                        { size ->
                            run {
                                var end = System.nanoTime()
                                var duration = (end - start)/1000000
                                Timber.d("sync finished size:${size} duration:${duration}")
                                syncStatus.text = "finished"
//                                syncPB.visibility = GONE
                            }
                        },
                        { err ->
                            run {
                                Timber.e("error", err)
                                syncStatus.text = "error"
                            }
                        }
                )
    }
}