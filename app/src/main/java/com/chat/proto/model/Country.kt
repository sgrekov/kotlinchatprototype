package com.chat.proto.model

import android.os.Parcelable
import io.requery.Entity
import io.requery.Generated
import io.requery.Key
import io.requery.Persistable


@Entity
interface Country : Parcelable, Persistable {
    @get:Key
    val id: Long
    var code: String
    var name: String
}
