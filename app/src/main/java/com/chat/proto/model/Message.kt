package com.chat.proto.model

import android.os.Parcelable
import io.requery.*
import java.util.*

@Entity
interface Message : Parcelable, Persistable {
    @get:Generated
    @get:Key
    var id: Long
    var replyId: Long?
    @get:ManyToOne
    var chat: Chat
    var text : String
    @get:ForeignKey
    @get:OneToOne
    var user : User
    var created : Date
}