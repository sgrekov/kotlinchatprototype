package com.chat.proto.model

import android.os.Parcelable
import io.requery.*
import java.util.*


@Entity
@Table(name = "ChatGroup")
interface Group :  Parcelable, Persistable {
    @get:Key
    val id : Long

    val uuid : UUID

    val name : String

    @get:JunctionTable
    @get:ManyToMany
    val users : MutableSet<User>
}