package com.chat.proto.model

import android.os.Parcelable
import io.requery.*
import java.util.*

@Entity
interface Chat : Parcelable, Persistable {
    @get:Key
    val id: Long
    val title: String?
    val created: Date
    val updated: Date
    val type : String
    @get:ManyToOne
    var chatGroup : Group?
    @get:JunctionTable
    @get:ManyToMany
    var users: MutableSet<User>
}