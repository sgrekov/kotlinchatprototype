package com.chat.proto.model

import android.os.Parcelable
import io.requery.*
import java.util.*


@Entity
interface User : Parcelable, Persistable {
    @get:Key
    val id: Long
    val name: String
    val phone : String
    val created: Date

    @get:ManyToOne
    var country : Country?
    @get:ManyToMany
    var groups : MutableSet<Group>?
    @get:ManyToMany
    var chats: MutableSet<Chat>?

}
