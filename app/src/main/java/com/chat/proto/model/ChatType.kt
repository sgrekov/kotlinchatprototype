package com.chat.proto.model


enum class ChatType(val code: String) {
    DIALOG("dialog"), CHAT("group_chat")
}