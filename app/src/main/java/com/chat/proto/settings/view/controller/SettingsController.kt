package com.chat.proto.settings.view.controller

import android.os.Bundle
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.Toolbar
import android.view.View
import com.chat.proto.R
import com.chat.proto.core.view.BaseController
import com.chat.proto.core.view.BaseInnerController


class SettingsController(toggle: ActionBarDrawerToggle?) : BaseInnerController(toggle) {

    constructor() : this(null) {
    }

    override fun title(): String? {
        return "Settings"
    }

    override fun layoutId(): Int = R.layout.settings_layout




}