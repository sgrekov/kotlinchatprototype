package com.chat.proto.chat.view.controller

import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.changehandler.FadeChangeHandler
import com.chat.proto.ChatAppKt
import com.chat.proto.R
import com.chat.proto.chat.view.scroll.KEndlessRecyclerOnScrollListener
import com.chat.proto.core.utility.EntityCreators
import com.chat.proto.core.utility.UserPreferences
import com.chat.proto.core.view.BaseController
import com.chat.proto.model.Chat
import com.chat.proto.model.ChatEntity
import com.chat.proto.model.Message
import com.chat.proto.model.MessageEntity
import com.chat.proto.thread.view.controller.ThreadController
import io.requery.Persistable
import io.requery.sql.KotlinEntityDataStore
import rx.Subscription
import timber.log.Timber
import java.util.*
import javax.inject.Inject


open class ChatController(val chat: Chat, val toggle: ActionBarDrawerToggle?) : BaseController() {

//    constructor(c: Chat, t: ActionBarDrawerToggle?) : this() {
//        chat = c
//        toggle = t
//    }

    constructor() : this(ChatEntity(), null) {
    }


//    var chat: Chat
//    var toggle: ActionBarDrawerToggle? = null
    lateinit var list: RecyclerView
    lateinit var emptyTextTV: TextView
    lateinit var inputText: EditText
    lateinit var sendBtn: Button
    lateinit var add100: Button
    lateinit var change100: Button
    @Inject
    lateinit var userPreferences: UserPreferences
    @Inject
    lateinit var dataStore: KotlinEntityDataStore<Persistable>
    var latestId: Long = 0
    var earliestId: Long = 0
    var subscription: Subscription? = null
    lateinit internal var adapter: MessageAdapter
    var pagesCount = 1

    companion object {
        val BUCKET = 60
    }

    override fun layoutId(): Int = R.layout.chat_layout

    override fun title(): String? = chat?.title

    override fun onViewBound(view: View) {
        super.onViewBound(view)

        ChatAppKt.graph.inject(this)

        emptyTextTV = view.findViewById(R.id.empty_list) as TextView
        list = view.findViewById(R.id.recycler_view) as RecyclerView
        inputText = view.findViewById(R.id.input_message) as EditText
        sendBtn = view.findViewById(R.id.sendBtn) as Button
        add100 = view.findViewById(R.id.addHundred) as Button
        change100 = view.findViewById(R.id.changeHundred) as Button
        val lm = LinearLayoutManager(view.context, LinearLayoutManager.VERTICAL, true)
        list.layoutManager = lm
        list.setOnScrollListener(object : KEndlessRecyclerOnScrollListener(lm) {
            override fun onLoadMore(currentPage: Int) {
                Timber.d("current page: ${currentPage}")
                pagesCount = currentPage
                subscribeToMessages()
            }
        })
        adapter = MessageAdapter(LayoutInflater.from(view.context), mutableListOf())
        adapter.setHasStableIds(true)
        list.adapter = adapter

        val otherUser = chat?.users?.filter { user -> user != userPreferences.loggedUser() }.first()
        add100.setOnClickListener {
            val messagesSet = (0..20L)
                    .map { EntityCreators.createMessage(it, "${chat?.title} message " + latestId++, chat!!, Date(), otherUser) }
                    .toMutableSet()
            dataStore.insert(messagesSet)
            list.adapter.notifyDataSetChanged()
        }

    }

    override fun onAttach(view: View) {
        super.onAttach(view)

        val ab = actionBar()

        ab?.setDisplayHomeAsUpEnabled(false)
        toggle?.setDrawerIndicatorEnabled(false)
        ab?.setDisplayHomeAsUpEnabled(true)
        toggle?.setToolbarNavigationClickListener {
            router.popCurrentController()
            toggle.setDrawerIndicatorEnabled(true)
        }


        subscribeToMessages()
    }

    open fun subscribeToMessages() {
        if (subscription?.isUnsubscribed ?: false) {
            subscription!!.unsubscribe()
        }

        subscription = dataStore.select(Message::class)
                .where(MessageEntity.CHAT_ID.eq(chat.id))
                .orderBy(MessageEntity.ID.desc())
                .limit(BUCKET * pagesCount)
                .get()
                .toSelfObservable()
                .subscribe(
                        { messages ->
                            run {
                                addMessages(messages.toList())
                            }
                        },
                        { err ->
                            run {
                                Log.e("", "error", err)
                                list.visibility = View.GONE
                                emptyTextTV.text = "Error happened"
                                emptyTextTV.visibility = View.VISIBLE
                            }
                        }
                )
    }


    fun addMessages(messages: MutableList<Message>) {
        Log.d("ChatController", "new messages size " + messages.size.toString())
        if (messages.size > 0) {
            adapter.setMessages(messages)
            emptyTextTV.visibility = View.GONE
            latestId = messages.first().id
            earliestId = messages.last().id
        } else {
            list.visibility = View.GONE
            emptyTextTV.visibility = View.VISIBLE
        }
    }

    override fun handleBack(): Boolean {
        toggle?.setDrawerIndicatorEnabled(true)
        return super.handleBack()
    }

    internal inner class MessageAdapter(private val inflater: LayoutInflater, private val items: MutableList<Message>) : RecyclerView.Adapter<MessageAdapter.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(inflater.inflate(R.layout.message_list_layout, parent, false))
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.bind(items[position])
        }

        override fun getItemCount(): Int {
            return items.size
        }

        override fun getItemId(position: Int): Long {
            return items.get(position).id
        }

        internal inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

            var messageTV: TextView
            var authorTV: TextView

            lateinit private var model: Message

            init {
                messageTV = itemView.findViewById(R.id.message) as TextView
                authorTV = itemView.findViewById(R.id.author) as TextView
            }

            fun bind(item: Message) {
                model = item
                if (model.replyId != null) {
                    Timber.d("text: ${model.text} replyId:${model.replyId}")
                    try {
                        val rootMsg = dataStore.select(Message::class)
                                .where(MessageEntity.CHAT_ID.eq(chat.id).and(MessageEntity.ID.eq(model.replyId)))
                                .get().first()
                        messageTV.text = "reply to:<${rootMsg.text}\n ${model.text}"
                        itemView.setOnClickListener {
                            openThread(rootMsg.id, rootMsg.text)
                        }
                    } catch (e: NoSuchElementException) {
                        Timber.e(e)
                        messageTV.text = "reply  to:<${model.replyId}\n ${model.text}"
                    }
                } else {
                    messageTV.text = model.text
                }
                authorTV.text = model.user.name
            }

        }

        fun setMessages(messages: MutableList<Message>) {
            this.items.clear()
            this.items.addAll(messages)
            notifyDataSetChanged()
        }
    }

    private fun openThread(rootId: Long, text: String) {
        router.pushController(RouterTransaction.with(ThreadController(rootId, text, chat, toggle))
                .pushChangeHandler(FadeChangeHandler())
                .popChangeHandler(FadeChangeHandler()))
    }

}