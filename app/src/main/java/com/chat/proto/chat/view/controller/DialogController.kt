package com.chat.proto.chat.view.controller

import android.os.Bundle
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.bluelinelabs.conductor.rxlifecycle2.ControllerEvent
import com.chat.proto.ChatAppKt
import com.chat.proto.R
import com.chat.proto.core.utility.EntityCreators
import com.chat.proto.core.utility.UserPreferences
import com.chat.proto.core.view.BaseController
import com.chat.proto.model.*
import io.reactivex.Observable
import io.requery.Persistable
import io.requery.sql.KotlinEntityDataStore
import rx.Subscription
import timber.log.Timber
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class DialogController(val chat: Chat, val toggle: ActionBarDrawerToggle?) : BaseController() {

//    init {
//        retainViewMode = RetainViewMode.RETAIN_DETACH
//    }

    lateinit var list: RecyclerView
    lateinit var emptyTextTV: TextView
    lateinit var inputText: EditText
    lateinit var fillGapp: Button
    lateinit var fill: Button
    lateinit var clear: Button
    @Inject
    lateinit var userPreferences: UserPreferences
    @Inject
    lateinit var dataStore: KotlinEntityDataStore<Persistable>
    var latestId: Long = 0
    var earliestId: Long = 0
    var subscription: Subscription? = null
    lateinit internal var adapter: MessageAdapter
    var pagesCount = 1
    var messagesList: MutableList<Message> = mutableListOf()
    var messagesList2: MutableList<Message> = mutableListOf()
    var messagesList3: MutableList<Message> = mutableListOf()

    companion object {
        val BUCKET = 60
    }

    constructor() : this(EntityCreators.createChat(0, "", ChatType.CHAT, Date(), null, mutableSetOf()), null) {
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        var v = super.onCreateView(inflater, container)
        Timber.d("onCreateView")
        retainViewMode = RetainViewMode.RETAIN_DETACH
        return v
    }

    override fun onDetach(view: View) {
        Timber.d("onDetach")
    }

    override fun onDestroy() {
        Timber.d("onDestroy")
    }

    override fun onSaveViewState(view: View, outState: Bundle) {
        Timber.d("onSaveViewState")
    }

    override fun layoutId(): Int = R.layout.dialog_layout

    override fun title(): String? {
        try {
            return chat.users.filter { user -> user != userPreferences.loggedUser() }.first().name
        } catch (e: NoSuchElementException) {
            Timber.e(e)
        }
        return "EEEEE"
    }

    override fun onViewBound(view: View) {
        super.onViewBound(view)

        ChatAppKt.graph.inject(this)

        emptyTextTV = view.findViewById(R.id.empty_list) as TextView
        list = view.findViewById(R.id.recycler_view) as RecyclerView
        inputText = view.findViewById(R.id.input_message) as EditText
        fill = view.findViewById(R.id.fillData) as Button
        clear = view.findViewById(R.id.clearData) as Button
        fillGapp = view.findViewById(R.id.filGap) as Button
        val lm = LinearLayoutManager(view.context, LinearLayoutManager.VERTICAL, true)
        list.layoutManager = lm
//        usersList.setOnScrollListener(object : KEndlessRecyclerOnScrollListener(lm) {
//            override fun onLoadMore(currentPage: Int) {
//                Timber.d("current page: ${currentPage}")
//                pagesCount = currentPage
//                subscribeToMessages()
//            }
//        })
        adapter = MessageAdapter(LayoutInflater.from(view.context), mutableListOf())
//        adapter.setHasStableIds(true)
        list.adapter = adapter

        var otherUser: User? = null;
        try {
            otherUser = chat.users.filter { user -> user != userPreferences.loggedUser() }.first()
        } catch (e: NoSuchElementException) {
            Timber.e(e)
        }
        if (otherUser == null) {
            return
        }
        var lastMessageId = dataStore.select(Message::class)
                .orderBy(MessageEntity.ID.desc())
                .limit(1)
                .get().first().id
        fill.setOnClickListener {
            messagesList = (0..29L)
                    .map { EntityCreators.createMessage(++lastMessageId, "message " + it, chat, Date(), otherUser!!) }
                    .toMutableList()
            var idWithDelta = lastMessageId + 30
            messagesList3 = (60..61L)
                    .map { EntityCreators.createMessage(idWithDelta++, "message " + it, chat, Date(), otherUser!!) }
                    .toMutableList()
//            dataStore.insert(messagesSet)
//            dataStore.insert(messagesSet2)
//            messagesList.addAll(messagesList2)
            adapter.addMessages(messagesList)
            adapter.addMessages(messagesList3)
            adapter.reverse()
            emptyTextTV.visibility = View.GONE
            list.adapter.notifyDataSetChanged()
        }

        clear.setOnClickListener {
            //            dataStore.delete(Message::class).where(MessageEntity.CHAT_ID.eq(chat.id)).get().value()
            adapter.clear()
            list.adapter.notifyDataSetChanged()
        }

        fillGapp.setOnClickListener {
            messagesList2 = (30..59L)
                    .map { EntityCreators.createMessage(lastMessageId++, "added message " + it, chat, Date(), otherUser!!) }
                    .toMutableList()
            adapter.addMessages(messagesList)
            adapter.addMessages(messagesList2)
            adapter.addMessages(messagesList3)
//            dataStore.insert(messagesSet3)
            adapter.reverse()
            adapter.notifyItemRangeInserted(0, messagesList2.size)
            for (i in 0..adapter.itemCount) {
                adapter.notifyItemChanged(i)
            }
        }

    }

    override fun onAttach(view: View) {
        super.onAttach(view)
        Timber.d("onAttach")

        Observable.interval(1, TimeUnit.SECONDS)
                .doOnDispose { Timber.d("Disposing from onAttach()") }
                .compose(this.bindUntilEvent<Long>(ControllerEvent.DETACH))
                .subscribe { num -> Timber.d("Started in onAttach(), running until onDetach(): " + num!!) }

        val ab = actionBar()

        ab?.setDisplayHomeAsUpEnabled(false)
        toggle?.setDrawerIndicatorEnabled(false)
        ab?.setDisplayHomeAsUpEnabled(true)
        toggle?.setToolbarNavigationClickListener {
            router.popCurrentController()
            toggle.setDrawerIndicatorEnabled(true)
        }

//        subscribeToMessages()
    }

    private fun subscribeToMessages() {
        if (subscription?.isUnsubscribed ?: false) {
            subscription!!.unsubscribe()
        }

        subscription = dataStore.select(Message::class)
                .where(MessageEntity.CHAT_ID.eq(chat.id))
                .orderBy(MessageEntity.ID.desc())
                .limit(BUCKET * pagesCount)
                .get()
                .toSelfObservable()
                .subscribe(
                        { messages ->
                            run {
                                addMessages(messages.toList())
                            }
                        },
                        { err ->
                            run {
                                Log.e("", "error", err)
                                list.visibility = View.GONE
                                emptyTextTV.text = "Error happened"
                                emptyTextTV.visibility = View.VISIBLE
                            }
                        }
                )
    }


    private fun addMessages(messages: MutableList<Message>) {
        Log.d("ChatController", "new messages size " + messages.size.toString())
        if (messages.size > 0) {
            adapter.setMessages(messages)
            emptyTextTV.visibility = View.GONE
            latestId = messages.first().id
            earliestId = messages.last().id
        } else {
            list.visibility = View.GONE
            emptyTextTV.visibility = View.VISIBLE
        }
    }

    override fun handleBack(): Boolean {
        toggle?.setDrawerIndicatorEnabled(true)
        return super.handleBack()
    }

    internal inner class MessageAdapter(private val inflater: LayoutInflater, private var items: MutableList<Message>) : RecyclerView.Adapter<MessageAdapter.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(inflater.inflate(R.layout.message_list_layout, parent, false))
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.bind(items[position])
        }

        override fun getItemCount(): Int {
            return items.size
        }

        override fun getItemId(position: Int): Long {
            return items.get(position).id
        }

        internal inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

            var messageTV: TextView
            var authorTV: TextView

            lateinit private var model: Message

            init {
                messageTV = itemView.findViewById(R.id.message) as TextView
                authorTV = itemView.findViewById(R.id.author) as TextView
            }

            fun bind(item: Message) {
                model = item
                messageTV.text = model.text
                authorTV.text = model.user.name
            }

        }

        fun addMessages(messages: MutableList<Message>) {
            this.items.addAll(messages)
        }

        fun setMessages(messages: MutableList<Message>) {
            this.items.clear()
            this.items.addAll(messages)
            notifyDataSetChanged()
        }

        fun clear() {
            this.items = mutableListOf()
        }

        fun reverse() {
            this.items.reverse()
        }
    }

}