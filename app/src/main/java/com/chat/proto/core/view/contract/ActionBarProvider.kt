package com.chat.proto.core.view.contract

import android.support.v7.app.ActionBar
import android.support.v7.widget.Toolbar

interface ActionBarProvider {
    fun getAppActionBar(): ActionBar?
    fun getToolbar(): Toolbar
}

