package com.chat.proto.core.view.contract

import android.support.design.widget.NavigationView
import android.support.v4.widget.DrawerLayout

interface NavBarProvider {
    fun getDrawer(): DrawerLayout
    fun getNavBar(): NavigationView
}
