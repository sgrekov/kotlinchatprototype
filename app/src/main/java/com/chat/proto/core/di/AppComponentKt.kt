package com.chat.proto.core.di

import com.chat.proto.ChatAppKt
import com.chat.proto.chat.view.controller.ChatController
import com.chat.proto.chat.view.controller.DialogController
import com.chat.proto.login.view.activity.LoginActivity
import com.chat.proto.login.view.controller.CountryListController
import com.chat.proto.login.view.controller.InputCodeController
import com.chat.proto.main.view.controller.ChatsController
import com.chat.proto.main.view.controller.DialogsController
import com.chat.proto.main.view.controller.HomeController
import com.chat.proto.profile.view.controller.ProfileController
import com.chat.proto.splash.view.activity.SplashActivity
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = arrayOf(AppModuleKt::class))
interface AppComponentKt {
    fun inject(chatAppKt: ChatAppKt)
    fun inject(activity: LoginActivity)
    fun inject(splashActivity: SplashActivity)
    fun inject(inputCodeController: InputCodeController)
    fun inject(homeController: HomeController)
    fun inject(profileController: ProfileController)
    fun inject(countryListController: CountryListController)
    fun inject(dialogsController: DialogsController)
    fun inject(chatsController: ChatsController)
    fun inject(chatController: ChatController)
    fun inject(dialogController: DialogController)

}