package com.chat.proto.core.utility

import com.chat.proto.model.*
import java.util.*


class EntityCreators {
    companion object {
        fun createGroup(id: Long, name: String, uuid: UUID?): Group {
            val g = GroupEntity()
            g.setId(id)
            g.setName(name)
            g.setUuid(uuid)
            return g
        }

        fun createUser(id: Long, name: String, phone: String, country: Country, date: Date, groups: MutableSet<Group>?): User {
            val user = UserEntity()
            user.setId(id)
            user.setName(name)
            user.setPhone(phone)
            user.country = country
            user.setCreated(date)
            if (groups != null) {
                user.groups = groups
            }
            return user
        }

        fun createCountry(id: Long, code: String, name: String): Country {
            val c = CountryEntity()
            c.setId(id)
            c.code = code
            c.name = name
            return c
        }

        fun createChat(id: Long, title: String?, type: ChatType, created: Date, group: Group?, users: MutableSet<User>): Chat {
            val c = ChatEntity()
            c.setId(id)
            c.setTitle(title)
            c.setCreated(created)
            c.setType(type.name)
            c.chatGroup = group
            c.users = users
            return c
        }

        fun createMessage(id: Long?, message: String, chat: Chat, date: Date, user: User, replyId : Long? = null): Message {
            val m = MessageEntity()
            if (id != null) m.id = id
            m.text = message
            m.chat = chat
            m.created = date
            m.user = user
            m.replyId = replyId
            return m
        }
    }

}