package com.chat.proto.core.utility

import android.content.SharedPreferences
import com.chat.proto.model.*
import io.requery.Persistable
import io.requery.sql.KotlinEntityDataStore
import java.util.*

val KEY_API_TOKEN_PREFIX = "api_key_prefix_"
val KEY_USER_ID_LOGGED = "key_user_id_logged"
val KEY_COUNTRIES_ARE_INITED = "key_countries_are_inited"

class UserPreferences(val preferences: SharedPreferences, val dataStore: KotlinEntityDataStore<Persistable>) {

    fun loginUser(user: User, apiKey: String) {
        val group1 = EntityCreators.createGroup(2L, "Hive Team", UUID.randomUUID())
        val group2 = EntityCreators.createGroup(3L, "GoT", UUID.randomUUID())
        val groupNoyabrsk = EntityCreators.createGroup(4L, "Ноябрьск", UUID.randomUUID())
        val groups = mutableSetOf<Group>(group1, group2, groupNoyabrsk)
        dataStore.insert(groups)
        val country1 = dataStore.findByKey(Country::class, 8)
        val country2 = dataStore.findByKey(Country::class, 10)

        val user2 = EntityCreators.createUser(2L, "Taiwin", "9991112233", country1, Date(), null)
        val user3 = EntityCreators.createUser(3L, "Bron", "1221112244", country2, Date(), null)
        val userSasha = EntityCreators.createUser(4L, "Sasha", "1221112244", country2, Date(), null)
        val userMasha = EntityCreators.createUser(5L, "Masha", "1221112244", country2, Date(), null)
        val userPetya = EntityCreators.createUser(6L, "Petya", "1221112244", country2, Date(), null)
        val userIra = EntityCreators.createUser(7L, "Ira", "1221112244", country2, Date(), null)
        dataStore.insert(setOf(user, user2, user3, userSasha, userMasha, userPetya, userPetya, userIra))
        user.groups = groups
        user2.groups = groups
        user3.groups = groups
        userSasha.groups = mutableSetOf(groupNoyabrsk)
        userMasha.groups = mutableSetOf(groupNoyabrsk)
        userPetya.groups = mutableSetOf(groupNoyabrsk)
        userIra.groups = mutableSetOf(groupNoyabrsk)

        dataStore.update(setOf(user, user2, user3, userSasha, userMasha, userPetya, userIra))
        val dialog1 = EntityCreators.createChat(1, null, ChatType.DIALOG, Date(), null, mutableSetOf(user, user2))
        val dialog2 = EntityCreators.createChat(2, null, ChatType.DIALOG, Date(), null, mutableSetOf(user, user3))
        val dialog3 = EntityCreators.createChat(100, null, ChatType.DIALOG, Date(), null, mutableSetOf(user2, user3))
        dataStore.insert(setOf(dialog1, dialog2, dialog3))
        val chat1 = EntityCreators.createChat(3, "Android Team", ChatType.CHAT, Date(), group1, mutableSetOf(user, user2, user3))
        val chat2 = EntityCreators.createChat(4, "iOS Team", ChatType.CHAT, Date(), group1, mutableSetOf(user, user2, user3))
        val chat3 = EntityCreators.createChat(5, "Королевская гавань", ChatType.CHAT, Date(), group2, mutableSetOf(user, user2, user3))
        val chat4 = EntityCreators.createChat(6, "Бобровый утес", ChatType.CHAT, Date(), group2, mutableSetOf(user, user2, user3))
        val chat5 = EntityCreators.createChat(7, "Стена", ChatType.CHAT, Date(), group2, mutableSetOf(user, user2, user3))
        val chat6 = EntityCreators.createChat(8, "Servar", ChatType.CHAT, Date(), group1, mutableSetOf(user, user2, user3))
        val chat7 = EntityCreators.createChat(9, "Design", ChatType.CHAT, Date(), group1, mutableSetOf(user2, user3))
        val chat8 = EntityCreators.createChat(10, "Общий", ChatType.CHAT, Date(), groupNoyabrsk, mutableSetOf(user, userSasha, userMasha, userPetya, userIra))
        val chat9 = EntityCreators.createChat(11, "Кино", ChatType.CHAT, Date(), groupNoyabrsk, mutableSetOf(user, userSasha, userMasha, userPetya, userIra))
        dataStore.insert(setOf(chat1, chat2, chat3, chat4, chat5, chat6, chat7, chat8, chat9))
        val messagesSet = (0..1000L)
                .map { EntityCreators.createMessage(it, "Android Team message " + it, chat1, Date(), user2) }
                .toMutableSet()
        dataStore.insert(messagesSet)

        dataStore.insert(EntityCreators.createMessage(null, "Всем привет, давайте кино обсуждать!", chat9, Date(), userSasha))
        dataStore.insert(EntityCreators.createMessage(null, "Какое?", chat9, Date(), userPetya))
        dataStore.insert(EntityCreators.createMessage(null, "Что сейчас идет?", chat9, Date(), userIra))
        dataStore.insert(EntityCreators.createMessage(null, "Кто нибудь пассажиры смотрел?", chat9, Date(), userIra))
        dataStore.insert(EntityCreators.createMessage(null, "Нет", chat9, Date(), userMasha))
        var rootMsg = EntityCreators.createMessage(null, "Я тут викинга посмотрел", chat9, Date(), userSasha)
        dataStore.insert(rootMsg)
        dataStore.insert(EntityCreators.createMessage(null, "Ну и как?", chat9, Date(), userMasha, rootMsg.id))
        dataStore.insert(EntityCreators.createMessage(null, "Говно редкостное", chat9, Date(), userSasha, rootMsg.id))
        dataStore.insert(EntityCreators.createMessage(null, "А я вот давно не ходил в кино ", chat9, Date(), userPetya))
        dataStore.insert(EntityCreators.createMessage(null, "Я тожееее", chat9, Date(), userIra))
        dataStore.insert(EntityCreators.createMessage(null, "Да у нас тут и ходить то некуда", chat9, Date(), userIra))
        dataStore.insert(EntityCreators.createMessage(null, "А мне понравилось!", chat9, Date(), userMasha, rootMsg.id))
        dataStore.insert(EntityCreators.createMessage(null, "У нас тоже(", chat9, Date(), userPetya))
        dataStore.insert(EntityCreators.createMessage(null, "Чем же?", chat9, Date(), userSasha, rootMsg.id))
        dataStore.insert(EntityCreators.createMessage(null, "Хоть в другой город в кино ездить((", chat9, Date(), userIra))
        dataStore.insert(EntityCreators.createMessage(null, "Козловский такой няшка", chat9, Date(), userMasha, rootMsg.id))
        dataStore.insert(EntityCreators.createMessage(null, "Бла бла бла бла ", chat9, Date(), userIra))
        dataStore.insert(EntityCreators.createMessage(null, "Бла бла бла бла ", chat9, Date(), userIra))
        dataStore.insert(EntityCreators.createMessage(null, "Бла бла бла бла ", chat9, Date(), userIra))
        dataStore.insert(EntityCreators.createMessage(null, "Бла бла бла бла ", chat9, Date(), userIra))
        dataStore.insert(EntityCreators.createMessage(null, "Бла бла бла бла ", chat9, Date(), userIra))
        var root2Msg = EntityCreators.createMessage(null, "Сходил на Пассажиров!", chat9, Date(), userPetya)
        dataStore.insert(root2Msg)
        dataStore.insert(EntityCreators.createMessage(null, "Бла бла бла бла ", chat9, Date(), userIra))
        dataStore.insert(EntityCreators.createMessage(null, "Бла бла бла бла ", chat9, Date(), userIra))
        dataStore.insert(EntityCreators.createMessage(null, "Бла бла бла бла ", chat9, Date(), userIra))
        dataStore.insert(EntityCreators.createMessage(null, "Я тоже", chat9, Date(), userIra, root2Msg.id))
        dataStore.insert(EntityCreators.createMessage(null, "Бла бла бла бла ", chat9, Date(), userIra))
        dataStore.insert(EntityCreators.createMessage(null, "Бла бла бла бла ", chat9, Date(), userIra))
        dataStore.insert(EntityCreators.createMessage(null, "Бла бла бла бла ", chat9, Date(), userIra))
        dataStore.insert(EntityCreators.createMessage(null, "Лоуренс клевая", chat9, Date(), userIra, root2Msg.id))
        dataStore.insert(EntityCreators.createMessage(null, "Бла бла бла бла ", chat9, Date(), userIra))
        dataStore.insert(EntityCreators.createMessage(null, "Бла бла бла бла ", chat9, Date(), userIra))
        dataStore.insert(EntityCreators.createMessage(null, "Бла бла бла бла ", chat9, Date(), userIra))
        dataStore.insert(EntityCreators.createMessage(null, "Робинзон Крузо в космосе", chat9, Date(), userPetya, root2Msg.id))
        dataStore.insert(EntityCreators.createMessage(null, "Бла бла бла бла ", chat9, Date(), userIra))
        dataStore.insert(EntityCreators.createMessage(null, "Бла бла бла бла ", chat9, Date(), userIra))
        dataStore.insert(EntityCreators.createMessage(null, "Бла бла бла бла ", chat9, Date(), userIra))
        dataStore.insert(EntityCreators.createMessage(null, "Бла бла бла бла ", chat9, Date(), userIra))
        dataStore.insert(EntityCreators.createMessage(null, "Бла бла бла бла ", chat9, Date(), userIra))
        dataStore.insert(EntityCreators.createMessage(null, "Бла бла бла бла ", chat9, Date(), userIra))
        dataStore.insert(EntityCreators.createMessage(null, "Бла бла бла бла ", chat9, Date(), userIra))



        val editor = preferences.edit()
        editor.putString(KEY_API_TOKEN_PREFIX + user.id, apiKey)
        editor.putLong(KEY_USER_ID_LOGGED, user.id)
        editor.apply()
    }


    fun loggedUser(): User? {
        if (isLogged()) {
            val user = dataStore.findByKey(User::class, preferences.getLong(KEY_USER_ID_LOGGED, 0L))
            return user
        } else {
            return null
        }
    }

    fun logout(): Boolean {
        val userId: Long = preferences.getLong(KEY_USER_ID_LOGGED, -1);
        if (userId == -1L) {
            return false
        }
        val editor = preferences.edit()
        editor.clear()
        editor.apply()
        deleteAllRecords()
        return true
    }

    private fun deleteAllRecords() {
        dataStore.delete(User::class).get().value()
        dataStore.delete(Country::class).get().value()
        dataStore.delete(Chat::class).get().value()
        dataStore.delete(Message::class).get().value()
        dataStore.delete(Group::class).get().value()
    }

    fun isLogged(): Boolean {
        val userId: Long = preferences.getLong(KEY_USER_ID_LOGGED, -1);
        if (userId == -1L) {
            return false
        }
        return preferences.getString(KEY_API_TOKEN_PREFIX + userId, null) != null
    }

    fun areContriesInited(): Boolean {
        return preferences.getBoolean(KEY_COUNTRIES_ARE_INITED, false)
    }

    fun countriesAreInited() {
        var editor = preferences.edit()
        editor.putBoolean(KEY_COUNTRIES_ARE_INITED, true)
        editor.apply()
    }
}
