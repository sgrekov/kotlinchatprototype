package com.chat.proto.core.view

import android.os.Bundle
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.Toolbar
import android.view.View
import java.util.*

/**
 * Created by serg on 21/01/2017.
 */
abstract class BaseInnerController(val toggle: ActionBarDrawerToggle?) : BaseController() {

    lateinit var toolbar: Toolbar

    override fun onAttach(view: View) {
        super.onAttach(view)
        val ab = actionBar()

        ab?.setDisplayHomeAsUpEnabled(false)
        toggle?.setDrawerIndicatorEnabled(false)
        ab?.setDisplayHomeAsUpEnabled(true)
        toggle?.setToolbarNavigationClickListener {
            router.popCurrentController()
            toggle?.setDrawerIndicatorEnabled(true)
        }
    }


    override fun handleBack(): Boolean {
        toggle?.setDrawerIndicatorEnabled(true)
        return super.handleBack()
    }

}