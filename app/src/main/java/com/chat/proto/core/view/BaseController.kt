package com.chat.proto.core.view

import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.v7.app.ActionBar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import bluelinelabs.conductor.butterknife.ViewBinder
import com.bluelinelabs.conductor.rxlifecycle2.RxController
import com.chat.proto.core.view.contract.ActionBarProvider

abstract class BaseController : RxController {

    constructor() {}

    constructor(args: Bundle) : super(args) {}

    protected fun actionBar(): ActionBar? {
        val actionBarProvider = activity as ActionBarProvider?
        return actionBarProvider?.getAppActionBar()
    }

    @CallSuper
    override fun onAttach(view: View) {
        setTitle()
        super.onAttach(view)
    }

    @CallSuper
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        val view = inflater.inflate(layoutId(), container, false)
        ViewBinder.setup(this, view)
        onViewBound(view)
        return view
    }

    @CallSuper
    open protected fun onViewBound(view: View){

    }

    protected abstract fun layoutId(): Int

    protected fun setTitle() {
        var parentController = parentController
        while (parentController != null) {
            if (parentController is BaseController && parentController.title() != null) {
                return
            }
            parentController = parentController.parentController
        }

        val title = title()
        val actionBar = actionBar()
        if (title != null && actionBar != null) {
            actionBar.title = title
        }
    }

    protected abstract fun title(): String?
}
