package com.chat.proto.core.di

import android.content.Context
import android.content.SharedPreferences
import android.location.LocationManager
import com.chat.proto.ChatAppKt
import com.chat.proto.core.utility.UserPreferences
import com.chat.proto.model.Models
import dagger.Module
import dagger.Provides
import io.requery.Persistable
import io.requery.android.sqlite.DatabaseSource
import io.requery.cache.EntityCacheBuilder
import io.requery.sql.*
import javax.inject.Named
import javax.inject.Singleton


@Module
class AppModuleKt(private val app: ChatAppKt) {

    @Provides
    @Singleton
    fun provideApplication(): ChatAppKt {
        return app
    }

    @Provides
    @Singleton
    fun provideDataStore(): KotlinEntityDataStore<Persistable> {
        val source = DatabaseSource(app, Models.DEFAULT, 1)
        source.setLoggingEnabled(true)
        source.setWriteAheadLoggingEnabled(true)
        source.setTableCreationMode(TableCreationMode.DROP_CREATE)
        val configuration = ConfigurationBuilder(source, Models.DEFAULT)
//            .useDefaultLogging()
//            .setEntityCache(EntityCacheBuilder(Models.DEFAULT)
//                .useReferenceCache(true)
//                .useSerializableCache(true)
//                .useCacheManager(cacheManager)
//                .build())
            .build()
        return KotlinEntityDataStore<Persistable>(configuration)
    }

    @Provides
    @Singleton
    fun provideEntityDataStore(): EntityDataStore<Persistable> {
        val source = DatabaseSource(app, Models.DEFAULT, 1)
        source.setLoggingEnabled(true)
        source.setWriteAheadLoggingEnabled(true)
        source.setTableCreationMode(TableCreationMode.DROP_CREATE)
        val configuration = ConfigurationBuilder(source, Models.DEFAULT)
//            .useDefaultLogging()
//            .setEntityCache(EntityCacheBuilder(Models.DEFAULT)
//                .useReferenceCache(true)
//                .useSerializableCache(true)
//                .useCacheManager(cacheManager)
//                .build())
                .build()
        return EntityDataStore<Persistable>(configuration)
    }

    @Provides
    @Singleton
    @Named("PerApplication")
    fun provideContext(): Context {
        return app.baseContext
    }

    @Provides
    @Singleton
    fun provideLocationManager(): LocationManager {
        return app.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    }

    @Provides
    @Singleton
    fun provideSharedPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences("chat", Context.MODE_PRIVATE)
    }

    @Provides
    @Singleton
    fun provideUserPreferences(): UserPreferences {
        return UserPreferences(provideSharedPreferences(provideContext()), provideDataStore())
    }
}