package com.chat.proto.profile.view.controller

import android.content.Intent
import android.support.v7.app.ActionBarDrawerToggle
import android.view.View
import android.widget.Button
import com.chat.proto.ChatAppKt
import com.chat.proto.R
import com.chat.proto.core.utility.UserPreferences
import com.chat.proto.core.view.BaseController
import com.chat.proto.login.view.activity.LoginActivity
import com.chat.proto.splash.view.activity.SplashActivity
import javax.inject.Inject


class ProfileController(val toggle: ActionBarDrawerToggle?) : BaseController() {

    lateinit var logoutBtn: Button
    @Inject
    lateinit var userPreferences: UserPreferences

    constructor() : this(null) {
    }

    override protected fun onViewBound(view: View){
        super.onViewBound(view)
        ChatAppKt.graph.inject(this)
    }

    override fun layoutId(): Int = R.layout.profile_layout

    override fun title(): String? = "Profile"

    override fun onAttach(view: View) {
        super.onAttach(view)
        logoutBtn = view.findViewById(R.id.logout) as Button
        logoutBtn.setOnClickListener {  run {
                userPreferences.logout()
                val intent = Intent(activity, SplashActivity::class.java)
                startActivity(intent)
                activity?.finish()
            }
        }

        val ab = actionBar()
        ab?.setDisplayHomeAsUpEnabled(false)
        toggle?.setDrawerIndicatorEnabled(false)
        ab?.setDisplayHomeAsUpEnabled(true)
        toggle?.setToolbarNavigationClickListener {
            router.popCurrentController()
            toggle.setDrawerIndicatorEnabled(true)
        }
    }

    override fun handleBack(): Boolean {
        toggle?.setDrawerIndicatorEnabled(true)
        return super.handleBack()
    }
}