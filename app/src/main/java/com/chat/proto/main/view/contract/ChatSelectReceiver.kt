package com.chat.proto.main.view.contract

import com.chat.proto.model.Chat


interface ChatSelectReceiver {

    fun onChatSelect(chat: Chat, isDialog: Boolean)

}