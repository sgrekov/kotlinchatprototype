package com.chat.proto.main.view.controller

import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Menu.NONE
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.changehandler.FadeChangeHandler
import com.bluelinelabs.conductor.changehandler.HorizontalChangeHandler
import com.chat.proto.ChatAppKt
import com.chat.proto.R
import com.chat.proto.bench.view.controller.BenchController
import com.chat.proto.chat.view.controller.ChatController
import com.chat.proto.chat.view.controller.DialogController
import com.chat.proto.core.utility.UserPreferences
import com.chat.proto.core.view.BaseController
import com.chat.proto.core.view.contract.ActionBarProvider
import com.chat.proto.core.view.contract.NavBarProvider
import com.chat.proto.layout.view.controller.LayoutController
import com.chat.proto.lifecycle.view.controller.LifecycleController
import com.chat.proto.main.view.contract.ChatSelectReceiver
import com.chat.proto.model.Chat
import com.chat.proto.model.Group
import com.chat.proto.profile.view.controller.ProfileController
import com.chat.proto.settings.view.controller.SettingsController
import io.requery.Persistable
import io.requery.sql.KotlinEntityDataStore
import javax.inject.Inject


class HomeController : BaseController(), NavigationView.OnNavigationItemSelectedListener, ChatSelectReceiver {

    init {
//        retainViewMode = RetainViewMode.RETAIN_DETACH
    }

    lateinit var toolbar: Toolbar
    lateinit var navigationView: NavigationView
    lateinit var drawerLayout: DrawerLayout
    lateinit var toggle: ActionBarDrawerToggle
    lateinit var detailContainer: ViewGroup

    @Inject
    lateinit var userPreferences: UserPreferences
    @Inject
    lateinit var dataStore: KotlinEntityDataStore<Persistable>

    override fun layoutId(): Int = R.layout.navigation_layout

    override fun onViewBound(view: View) {
        super.onViewBound(view)
        Log.d("HomeController", "onViewBound")

        ChatAppKt.graph.inject(this)

        detailContainer = view.findViewById(R.id.detail_container) as ViewGroup

        if (activity is ActionBarProvider) {
            toolbar = (activity as ActionBarProvider).getToolbar()
        }
        if (activity is NavBarProvider) {
            drawerLayout = (activity as NavBarProvider).getDrawer()
            navigationView = (activity as NavBarProvider).getNavBar()
            navigationView.menu.add(NONE, 1, 0, "All groups")
            val user = userPreferences.loggedUser()
            var i = 1
            user?.groups?.forEach {
                navigationView.menu.add(NONE, it.id.toInt(), i++, it.name)
            }

            val subMenu = navigationView.menu.addSubMenu(NONE, 6, 6, "Others")
            subMenu.add(NONE, 7, 7, "Profile")
            subMenu.add(NONE, 8, 8, "Settings")
            subMenu.add(NONE, 9, 9, "Layout")
            subMenu.add(NONE, 10, 10, "Bench")
            subMenu.add(NONE, 11, 11, "RxLifecycle")

            val userName = navigationView.getHeaderView(0).findViewById(R.id.nav_user_name) as TextView
            val userPhone = navigationView.getHeaderView(0).findViewById(R.id.nav_user_phone) as TextView
            userName.setText(userPreferences.loggedUser()?.name)
            userPhone.setText(userPreferences.loggedUser()?.country?.code + " " + userPreferences.loggedUser()?.phone)
        }

    }

    override fun onAttach(view: View) {
        super.onAttach(view)
        toggle = ActionBarDrawerToggle(
                activity, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        toggle.setDrawerIndicatorEnabled(true)
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        navigationView.setNavigationItemSelectedListener(this)

        val controller = RecentController(null, this)
        router.pushController(RouterTransaction.with(controller))
//        router.pushController(RouterTransaction.with(LayoutController(toggle)))
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            Log.d("InputPhoneController", "onOptionsItemSelected")
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onChatSelect(chat: Chat, isDialog: Boolean) {
        if (isDialog){
            router.pushController(RouterTransaction.with(DialogController(chat, toggle))
                    .pushChangeHandler(FadeChangeHandler())
                    .popChangeHandler(FadeChangeHandler()))
        }else {
            router.pushController(RouterTransaction.with(ChatController(chat, toggle))
                    .pushChangeHandler(FadeChangeHandler())
                    .popChangeHandler(FadeChangeHandler()))
        }

    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                router.popCurrentController()
                return true
            }
            1 -> {
                drawerLayout.closeDrawer(GravityCompat.START)

                val controller = RecentController(null, this)
                router.replaceTopController(RouterTransaction.with(controller))
                toolbar.setTitle("All groups")
                return true
            }
            8 -> {
                drawerLayout.closeDrawer(GravityCompat.START)
                router.pushController(RouterTransaction.with(SettingsController(toggle))
                        .pushChangeHandler(HorizontalChangeHandler()))
            }
            7 -> {
                drawerLayout.closeDrawer(GravityCompat.START)
                router.pushController(RouterTransaction.with(ProfileController(toggle))
                        .pushChangeHandler(HorizontalChangeHandler()))
            }
            9 -> {
                drawerLayout.closeDrawer(GravityCompat.START)
                router.pushController(RouterTransaction.with(LayoutController(toggle))
                        .pushChangeHandler(HorizontalChangeHandler()))
            }
            10 -> {
                drawerLayout.closeDrawer(GravityCompat.START)
                router.pushController(RouterTransaction.with(BenchController(toggle))
                        .pushChangeHandler(HorizontalChangeHandler()))
            }
            11 -> {
                drawerLayout.closeDrawer(GravityCompat.START)
                router.pushController(RouterTransaction.with(LifecycleController())
                        .pushChangeHandler(HorizontalChangeHandler()))
            }
            else -> {
                drawerLayout.closeDrawer(GravityCompat.START)
                val group = dataStore.findByKey(Group::class, item.itemId)
                if (group != null){
                    val controller = RecentController(group, this)
                    router.replaceTopController(RouterTransaction.with(controller))
                }
                return true
            }
        }

        return false
    }

    override fun onDetach(view: View) {
//        Log.d("ss", "sdfs")
    }

    override fun title(): String? {
        return "Home: All groups"
    }
}
