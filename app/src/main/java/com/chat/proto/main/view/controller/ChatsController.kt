package com.chat.proto.main.view.controller

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.changehandler.FadeChangeHandler
import com.chat.proto.ChatAppKt
import com.chat.proto.R
import com.chat.proto.chat.view.controller.ChatController
import com.chat.proto.core.utility.UserPreferences
import com.chat.proto.core.view.BaseController
import com.chat.proto.main.view.contract.ChatSelectReceiver
import com.chat.proto.model.Chat
import com.chat.proto.model.ChatEntity
import com.chat.proto.model.ChatType
import com.chat.proto.model.Group
import io.requery.Persistable
import io.requery.sql.EntityDataStore
import io.requery.sql.KotlinEntityDataStore
import javax.inject.Inject


class ChatsController(val group: Group?, val chatSelectReceiver: ChatSelectReceiver?) : BaseController() {

    lateinit var list: RecyclerView
    lateinit var emptyTextTV: TextView

    @Inject
    lateinit var userPreferences: UserPreferences
    @Inject
    lateinit var dataStore: KotlinEntityDataStore<Persistable>

    @Inject
    lateinit var javaDataStore: EntityDataStore<Persistable>

    constructor() : this(null, null) {
    }

    override fun layoutId(): Int = R.layout.channels_layout

    override fun title(): String? = "Channels"

    override fun onViewBound(view: View) {
        super.onViewBound(view)

        emptyTextTV = view.findViewById(R.id.empty_list) as TextView
        list = view.findViewById(R.id.recycler_view) as RecyclerView
        val lm = LinearLayoutManager(view.context, LinearLayoutManager.VERTICAL, false)
        list.layoutManager = lm

        ChatAppKt.graph.inject(this)
    }

    override fun onAttach(view: View) {
        super.onAttach(view)

        val user = userPreferences.loggedUser()
        val chats = (dataStore select (Chat::class) where (
                if (group != null)
                    (ChatEntity.TYPE.eq(ChatType.CHAT.name)
                            .and(ChatEntity.CHAT_GROUP.notNull())
                            .and(ChatEntity.CHAT_GROUP.eq(group)))
                else
                    (ChatEntity.TYPE.eq(ChatType.CHAT.name)
                            .and(ChatEntity.CHAT_GROUP.notNull())))
                ).get().toList().filter { it.users.contains(user) }

        if (chats != null && chats.size > 0) {
            list.adapter = ChatsAdapter(LayoutInflater.from(view.context), chats)
            emptyTextTV.visibility = View.GONE
        } else {
            list.visibility = View.GONE
            emptyTextTV.visibility = View.VISIBLE
        }
    }

    internal inner class ChatsAdapter(private val inflater: LayoutInflater, private val items: List<Chat>) : RecyclerView.Adapter<ChatsAdapter.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(inflater.inflate(R.layout.dialog_list_layout, parent, false))
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.bind(items[position])
        }

        override fun getItemCount(): Int {
            return items.size
        }

        internal inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

            var tvTitle: TextView

            lateinit private var model: Chat

            init {
                tvTitle = itemView.findViewById(R.id.dialog_title_tv) as TextView
            }

            fun bind(item: Chat) {
                model = item
                if (item.title != null) {
                    tvTitle.text = item.title
                } else {
                    tvTitle.text = "${item.id} _____________"
                }

                itemView.setOnClickListener {
                    chatSelectReceiver?.onChatSelect(model, false)
                }
            }


        }
    }


}