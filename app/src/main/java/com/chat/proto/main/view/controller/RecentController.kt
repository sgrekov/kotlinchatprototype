package com.chat.proto.main.view.controller

import android.support.design.widget.BottomNavigationView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.ViewGroup
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.changehandler.FadeChangeHandler
import com.chat.proto.R
import com.chat.proto.core.view.BaseController
import com.chat.proto.main.view.contract.ChatSelectReceiver
import com.chat.proto.model.Group


class RecentController(val group: Group?, val selectReceiver: ChatSelectReceiver?) : BaseController() {

    init {
//        setRetainViewMode(RetainViewMode.RETAIN_DETACH)
    }

    lateinit var bottomNavigationView: BottomNavigationView

    constructor() : this(null, null) {
    }

    lateinit var recyclerView: RecyclerView
    lateinit var lm: LinearLayoutManager
    lateinit var contentContainer: ViewGroup
    lateinit var childRouter: Router

    override fun layoutId(): Int = com.chat.proto.R.layout.recent_layout

    override fun onViewBound(view: View) {
        super.onViewBound(view)
        contentContainer = view.findViewById(R.id.content_controller_container) as ViewGroup
        childRouter = getChildRouter(contentContainer)

    }

    override fun onAttach(view: View) {
        super.onAttach(view)
        Log.d("RecentController", "onAttach")

        childRouter.setRoot(RouterTransaction.with(ChatsController(group, selectReceiver))
                .pushChangeHandler(FadeChangeHandler())
                .popChangeHandler(FadeChangeHandler()))

        bottomNavigationView = view.findViewById(com.chat.proto.R.id.bottom_navigation) as BottomNavigationView
        bottomNavigationView.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.action_recent -> {
                    childRouter.setRoot(RouterTransaction.with(ChatsController(group, selectReceiver))
                            .pushChangeHandler(FadeChangeHandler())
                            .popChangeHandler(FadeChangeHandler()))
                }
                R.id.action_messages -> {
                    childRouter.setRoot(RouterTransaction.with(DialogsController(selectReceiver))
                            .pushChangeHandler(FadeChangeHandler())
                            .popChangeHandler(FadeChangeHandler()))
                }
            }
            true
        }
    }

    override fun onDetach(view: View) {
//        Log.d("ss", "sdfs")
    }


    override fun title(): String? {
        return group?.name ?: "All groups"
    }
}


