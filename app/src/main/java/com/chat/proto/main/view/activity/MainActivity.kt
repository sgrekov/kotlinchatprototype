package com.chat.proto.main.view.activity

import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import com.bluelinelabs.conductor.Conductor
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import com.chat.proto.R
import com.chat.proto.core.view.contract.ActionBarProvider
import com.chat.proto.core.view.contract.NavBarProvider
import com.chat.proto.main.view.controller.HomeController
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.home_layout.*
import timber.log.Timber

class MainActivity : AppCompatActivity(), ActionBarProvider, NavBarProvider, NavigationView.OnNavigationItemSelectedListener {

    lateinit private var router: Router

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)

        router = Conductor.attachRouter(this, controllerContainer, savedInstanceState)
        if (!router.hasRootController()) {
            router.setRoot(RouterTransaction.with(HomeController()))
        }
    }

    override fun onBackPressed() {
        if (!router.handleBack()) {
            super.onBackPressed()
        }
    }


    override fun onPause(){
        super.onPause()
        Timber.d("MainActivity onPause")
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        return false
    }

    override fun getAppActionBar(): ActionBar? {
        return supportActionBar
    }

    override fun getToolbar(): Toolbar {
        return toolbar
    }

    override fun getDrawer(): DrawerLayout {
        return drawerLayoutMain
    }

    override fun getNavBar(): NavigationView {
        return navigationViewMain
    }
}
