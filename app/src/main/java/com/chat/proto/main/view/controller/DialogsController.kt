package com.chat.proto.main.view.controller

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.chat.proto.ChatAppKt
import com.chat.proto.R
import com.chat.proto.core.utility.UserPreferences
import com.chat.proto.core.view.BaseController
import com.chat.proto.main.view.contract.ChatSelectReceiver
import com.chat.proto.model.Chat
import com.chat.proto.model.ChatEntity
import com.chat.proto.model.ChatType
import com.chat.proto.model.User
import io.requery.Persistable
import io.requery.sql.KotlinEntityDataStore
import javax.inject.Inject


class DialogsController() : BaseController() {

    lateinit var list: RecyclerView
    lateinit var emptyTextTV: TextView
    @Inject
    lateinit var userPreferences: UserPreferences
    @Inject
    lateinit var dataStore: KotlinEntityDataStore<Persistable>
    var selectReceiver: ChatSelectReceiver? = null


    constructor(selectReceiver: ChatSelectReceiver?) : this() {
        this.selectReceiver = selectReceiver
    }

    override fun title(): String? = "Messages"

    override fun layoutId(): Int = R.layout.dialogs_layout

    override fun onViewBound(view: View) {
        super.onViewBound(view)

        ChatAppKt.graph.inject(this)

        emptyTextTV = view.findViewById(R.id.empty_list) as TextView
        list = view.findViewById(R.id.recycler_view) as RecyclerView
        val lm = LinearLayoutManager(view.context, LinearLayoutManager.VERTICAL, false)
        list.layoutManager = lm
    }

    override fun onAttach(view: View) {
        super.onAttach(view)

        val user = userPreferences.loggedUser()
        val dialogs = (dataStore select (Chat::class) where (ChatEntity.TYPE.eq(ChatType.DIALOG.name)) ).get().toList()
                .filter { it.users.contains(user) }

        if (dialogs != null && dialogs.size > 0) {
            list.adapter = DialogsAdapter(LayoutInflater.from(view.context), dialogs)
            emptyTextTV.visibility = View.GONE
        } else {
            list.visibility = View.GONE
            emptyTextTV.visibility = View.VISIBLE
        }
    }

    internal inner class DialogsAdapter(private val inflater: LayoutInflater, private val items: List<Chat>) : RecyclerView.Adapter<DialogsAdapter.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(inflater.inflate(R.layout.dialog_list_layout, parent, false))
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.bind(items[position])
        }

        override fun getItemCount(): Int {
            return items.size
        }

        internal inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

            var tvTitle: TextView

            lateinit private var model: Chat
            val author : User? = userPreferences.loggedUser()

            init {
                tvTitle = itemView.findViewById(R.id.dialog_title_tv) as TextView
            }

            fun bind(item: Chat) {
                model = item
                val oponentName = model.users.filter { user -> user.id != author?.id }.last().name
                tvTitle.text =  "${oponentName}:"

                itemView.setOnClickListener {
                    selectReceiver?.onChatSelect(model, true)
                }
            }


        }
    }
}