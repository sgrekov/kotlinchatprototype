package com.chat.proto.login.view.controller

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import com.chat.proto.ChatAppKt
import com.chat.proto.R
import com.chat.proto.core.utility.EntityCreators
import com.chat.proto.core.utility.UserPreferences
import com.chat.proto.core.view.BaseController
import com.chat.proto.model.Country
import io.requery.Persistable
import io.requery.sql.KotlinEntityDataStore
import javax.inject.Inject

class CountryListController(fromController: InputPhoneController?) : BaseController() {

    init {
        targetController = fromController
    }

    constructor() : this(null) {
    }

    interface TargetCountrySelectListener {
        fun onCountryPicked(country: Country)
    }

    lateinit var recyclerView: RecyclerView
    lateinit var lm: LinearLayoutManager
    @Inject
    lateinit var data: KotlinEntityDataStore<Persistable>
    @Inject
    lateinit var userPref: UserPreferences

    override fun layoutId(): Int = R.layout.country_list_layout

    override fun onAttach(view: View) {
        super.onAttach(view)

        val imm = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm?.hideSoftInputFromWindow(view.windowToken, 0)
    }

    override fun onViewBound(view: View) {
        super.onViewBound(view)

        ChatAppKt.graph.inject(this)

        recyclerView = view.findViewById(R.id.recycler_view) as RecyclerView
        lm = LinearLayoutManager(view.context, LinearLayoutManager.VERTICAL, true)
        recyclerView.layoutManager = lm

        if (!userPref.areContriesInited()) {
            val countriesToSave = setOf<Country>(
                    EntityCreators.createCountry(1, "+7", "Russia"),
                    EntityCreators.createCountry(2, "+6", "Belorussia"),
                    EntityCreators.createCountry(3, "+5", "Ukraine"),
                    EntityCreators.createCountry(4, "+4", "China"),
                    EntityCreators.createCountry(5, "+300", "Neitherlands"),
                    EntityCreators.createCountry(6, "+300", "Germany"),
                    EntityCreators.createCountry(7, "+400", "Italy"),
                    EntityCreators.createCountry(8, "+500", "Britain"),
                    EntityCreators.createCountry(9, "+600", "France"),
                    EntityCreators.createCountry(10, "+700", "Uzbekistan"),
                    EntityCreators.createCountry(11, "+800", "Kazahstan"),
                    EntityCreators.createCountry(12, "+1", "USA"),
                    EntityCreators.createCountry(13, "+2", "Canada"),
                    EntityCreators.createCountry(14, "+3", "Mexico"),
                    EntityCreators.createCountry(15, "+4", "Belgium"),
                    EntityCreators.createCountry(16, "+5", "Poland"),
                    EntityCreators.createCountry(17, "+6", "Denmark"),
                    EntityCreators.createCountry(18, "+7", "Sweden"),
                    EntityCreators.createCountry(19, "+8", "Norway"),
                    EntityCreators.createCountry(20, "+9", "Finland"),
                    EntityCreators.createCountry(21, "+7", "Estonia"),
                    EntityCreators.createCountry(22, "+11", "Lithunia"))
            data.insert(countriesToSave)
            userPref.countriesAreInited()
        }
        val countries = (data select (Country::class)).get().toList()
        recyclerView.adapter = CountriesAdapter(LayoutInflater.from(view.context), countries)
    }


    override fun title(): String? = "Countries"

    private fun onCountryRowClick(model: Country) {
        val target = targetController as? TargetCountrySelectListener
        target?.onCountryPicked(model)
        router?.popController(this)
    }

    internal inner class CountriesAdapter(private val inflater: LayoutInflater, private val items: List<Country>) : RecyclerView.Adapter<CountriesAdapter.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(inflater.inflate(R.layout.row_home, parent, false))
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.bind(items[position])
        }

        override fun getItemCount(): Int {
            return items.size
        }

        internal inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

            var tvTitle: TextView
            var tvCode: TextView
            lateinit private var model: Country

            init {
                tvTitle = itemView.findViewById(R.id.country_title_tv) as TextView
                tvCode = itemView.findViewById(R.id.country_code_tv) as TextView
            }

            fun bind(item: Country) {
                model = item
                tvTitle.text = item.name
                tvCode.text = item.code
                itemView.setOnClickListener {
                    onCountryRowClick(model)
                }
            }


        }
    }

}


