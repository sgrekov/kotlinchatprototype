package com.chat.proto.login.view.activity

import android.location.LocationManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.ActionBar
import android.support.v7.widget.Toolbar
import com.bluelinelabs.conductor.Conductor
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import com.chat.proto.ChatAppKt
import com.chat.proto.R
import com.chat.proto.core.utility.UserPreferences
import com.chat.proto.core.view.contract.ActionBarProvider
import com.chat.proto.login.view.controller.InputPhoneController
import kotlinx.android.synthetic.main.home_layout.*
import javax.inject.Inject

class LoginActivity : AppCompatActivity(), ActionBarProvider {


    override fun getAppActionBar(): ActionBar? {
        return supportActionBar
    }

    override fun getToolbar(): Toolbar {
        return toolbar
    }

    lateinit private var router: Router
    @Inject
    lateinit var userPreferences: UserPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        setSupportActionBar(toolbar)


        ChatAppKt.graph.inject(this)

        router = Conductor.attachRouter(this, controllerContainer, savedInstanceState)
        if (!router.hasRootController()) {
            router.setRoot(RouterTransaction.with(InputPhoneController()))
        }
    }

    override fun onBackPressed() {
        if (!router.handleBack()) {
            super.onBackPressed()
        }
    }

}
