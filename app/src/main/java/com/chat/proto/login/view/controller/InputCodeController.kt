package com.chat.proto.login.view.controller

import android.content.Context
import android.content.Intent
import android.support.design.widget.Snackbar
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import com.bluelinelabs.conductor.ControllerChangeHandler
import com.bluelinelabs.conductor.ControllerChangeType
import com.chat.proto.ChatAppKt
import com.chat.proto.R
import com.chat.proto.core.utility.EntityCreators
import com.chat.proto.core.utility.UserPreferences
import com.chat.proto.core.view.BaseController
import com.chat.proto.main.view.activity.MainActivity
import com.chat.proto.model.Country
import com.chat.proto.model.CountryEntity
import java.util.*
import javax.inject.Inject

class InputCodeController(val phone: String, val country: Country) : BaseController() {
    lateinit var inputCodeEdt: EditText
    lateinit var inputCodePhoneLabel: TextView
    lateinit var rootView: ViewGroup
    @Inject
    lateinit var userPreferences: UserPreferences

    init {
        setHasOptionsMenu(true)
    }

    constructor() : this("", CountryEntity()) {
    }

    override fun layoutId(): Int = R.layout.input_pin_layout

    override fun onAttach(view: View) {
        super.onAttach(view)

        ChatAppKt.graph.inject(this)

        val ab = actionBar()
        ab!!.setDisplayHomeAsUpEnabled(true)
        //        ab.setDisplayShowHomeEnabled(true);
        ab.setHomeButtonEnabled(true)

        inputCodeEdt = view.findViewById(R.id.input_code_code) as EditText
        inputCodePhoneLabel = view.findViewById(R.id.input_code_phone_label) as TextView
        rootView = view.findViewById(R.id.root_view) as ViewGroup

        inputCodePhoneLabel.setText(country.code + " " + phone)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.input_code, menu)
    }

    override fun onChangeStarted(changeHandler: ControllerChangeHandler, changeType: ControllerChangeType) {
        setOptionsMenuHidden(!changeType.isEnter)

        if (changeType.isEnter) {
            setTitle()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.enter) {
            val imm = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm?.hideSoftInputFromWindow(rootView.windowToken, 0)

            val code = inputCodeEdt.text.toString()
            if (code.isBlank()) {
                Snackbar.make(rootView, "Enter the code please!", Snackbar.LENGTH_LONG).show()
                return true
            }
            if (!code.equals(phone.takeLast(4))) {
                Snackbar.make(rootView, "Code is not equals!", Snackbar.LENGTH_LONG).show()
                return true
            }


            userPreferences.loginUser(EntityCreators.createUser(1L, "Tirion", phone, country, Date(), null), "qqqq")

            val intent = Intent(applicationContext, MainActivity::class.java)
            startActivity(intent)
            activity?.finish()
            return true
        } else if (item.itemId == android.R.id.home) {
            router.popCurrentController()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun title(): String? {
        return "Code"
    }
}
