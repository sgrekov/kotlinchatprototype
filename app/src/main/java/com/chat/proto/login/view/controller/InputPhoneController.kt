package com.chat.proto.login.view.controller


import android.content.Context
import android.support.design.widget.Snackbar
import android.support.v7.app.ActionBarDrawerToggle
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import com.bluelinelabs.conductor.ControllerChangeHandler
import com.bluelinelabs.conductor.ControllerChangeType
import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.changehandler.FadeChangeHandler
import com.bluelinelabs.conductor.changehandler.HorizontalChangeHandler
import com.bluelinelabs.conductor.changehandler.VerticalChangeHandler
import com.chat.proto.R
import com.chat.proto.core.view.BaseController
import com.chat.proto.model.Country

class InputPhoneController : BaseController, CountryListController.TargetCountrySelectListener {

    //    val inputPhoneCountryEdt: EditText by bindView(R.id.inputPhoneCountryEdt)
//    val inputPhoneCodeEdt: EditText by bindView(R.id.inputPhoneCodeEdt)
//    val inputPhoneEdt: EditText by bindView(R.id.inputPhoneEdt)
    lateinit var inputPhoneCountryEdt: EditText
    lateinit var inputPhoneCodeEdt: EditText
    lateinit var inputPhoneEdt: EditText
    lateinit var rootView: ViewGroup
    var country: Country? = null

    constructor() {
        setHasOptionsMenu(true)
    }

    constructor(toggle: ActionBarDrawerToggle) {
        setHasOptionsMenu(true)
    }

    override fun onCountryPicked(c: Country) {
        country = c
//        inputPhoneCountryEdt.setText(c.name)
//        inputPhoneCodeEdt.setText(c.code)
    }

    override fun layoutId(): Int = R.layout.input_phone_layout

    override fun onAttach(view: View) {
        super.onAttach(view)

        inputPhoneCountryEdt = view.findViewById(R.id.inputPhoneCountryEdt) as EditText
        inputPhoneCodeEdt = view.findViewById(R.id.inputPhoneCodeEdt) as EditText
        inputPhoneEdt = view.findViewById(R.id.inputPhoneEdt) as EditText
        rootView = view.findViewById(R.id.phone_root_view) as ViewGroup

        inputPhoneCountryEdt.setOnClickListener {
            Log.d("InputPhoneController", "inputPhoneCountryEdt.setOnClickListener")
            router.pushController(RouterTransaction.with(CountryListController(this))
                    .pushChangeHandler(VerticalChangeHandler())
                    .popChangeHandler(VerticalChangeHandler()))
        }

        inputPhoneCodeEdt.setOnClickListener {
            Log.d("InputPhoneController", "inputPhoneCodeEdt.setOnClickListener")
            router.pushController(RouterTransaction.with(CountryListController(this))
                    .pushChangeHandler(HorizontalChangeHandler())
                    .popChangeHandler(HorizontalChangeHandler()))
        }

        inputPhoneCodeEdt.setText(country?.code)
        inputPhoneCountryEdt.setText(country?.name)

        val ab = actionBar()
        ab!!.setDisplayHomeAsUpEnabled(false)
        //        ab.setDisplayShowHomeEnabled(true);
        ab.setHomeButtonEnabled(false)

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        Log.d("InputPhoneController", "onCreateOptionsMenu")
        inflater.inflate(R.menu.input_phone, menu)
        val next = menu.findItem(R.id.next)
    }

    override fun onChangeStarted(changeHandler: ControllerChangeHandler, changeType: ControllerChangeType) {
        setOptionsMenuHidden(!changeType.isEnter)

        if (changeType.isEnter) {
            setTitle()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.next) {
            val imm = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm?.hideSoftInputFromWindow(rootView.windowToken, 0)

            val phone = inputPhoneEdt.text
            if (phone.isBlank()){
                Snackbar.make(rootView, "Enter the phone please!", Snackbar.LENGTH_LONG).show()
                return true
            }

            if (country == null){
                Snackbar.make(rootView, "Choose the country please!", Snackbar.LENGTH_LONG).show()
                return true
            }
            router.pushController(RouterTransaction.with(InputCodeController(phone.toString(), country!!))
                    .pushChangeHandler(FadeChangeHandler())
                    .popChangeHandler(FadeChangeHandler()))
            return true
        } else if (item.itemId == android.R.id.home) {
            //            drawerLayout.openDrawer(navigationView);
            Log.d("InputPhoneController", "onOptionsItemSelected")
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun title() = "Login Demo"

}
