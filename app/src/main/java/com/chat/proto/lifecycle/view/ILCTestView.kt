package com.chat.proto.lifecycle.view

interface ILCTestView {
    fun setBtnText(s: String)
    fun setText(s: String)
    fun getText(): Any
}