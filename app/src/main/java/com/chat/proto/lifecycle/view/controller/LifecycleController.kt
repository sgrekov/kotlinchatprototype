package com.chat.proto.lifecycle.view.controller

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.airbnb.rxgroups.GroupLifecycleManager
import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.changehandler.HorizontalChangeHandler
import com.chat.proto.ChatAppKt
import com.chat.proto.R
import com.chat.proto.core.view.BaseController
import com.chat.proto.lifecycle.presenter.LCPresenter
import com.chat.proto.lifecycle.view.ILCTestView
import timber.log.Timber

class LifecycleController() : BaseController(), ILCTestView {


    lateinit var presenter : LCPresenter

    private var groupLifecycleManager: GroupLifecycleManager? = null
    lateinit var startBtn: Button
    lateinit var nextScreenBtn: Button
    lateinit var timerResult: TextView

    override fun setBtnText(s: String) {
        startBtn.text = s
    }

    override fun setText(s: String) {
        timerResult.text = s
    }

    override fun getText(): Any {
        return timerResult.text
    }

    override fun layoutId(): Int = R.layout.lifecycle_layout

    override fun title(): String? = "Rx Lifecycle Tests"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        val v = super.onCreateView(inflater, container)
        Timber.d("onCreateView")

        if (groupLifecycleManager == null) {
            val app = activity?.application as ChatAppKt
            val manager = app.observableManager()
            groupLifecycleManager = GroupLifecycleManager.onCreate(manager, null, this)
            presenter = LCPresenter(this, groupLifecycleManager)
        }

        retainViewMode = RetainViewMode.RELEASE_DETACH
        startBtn = v.findViewById(R.id.startBtn) as Button
        startBtn.setOnClickListener {
            presenter.startTimers()

        }
        timerResult = v.findViewById(R.id.timer_result) as TextView
        nextScreenBtn = v.findViewById(R.id.nextScreenBtn) as Button
        nextScreenBtn.setOnClickListener {
            router.pushController(RouterTransaction.with(NextLifeCycleController())
                    .pushChangeHandler(HorizontalChangeHandler()))
        }
        return v
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        val app = activity?.application as ChatAppKt
        val manager = app.observableManager()
        groupLifecycleManager = GroupLifecycleManager.onCreate(manager, savedInstanceState, this)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        groupLifecycleManager?.onSaveInstanceState(outState)
    }

    override fun onDetach(view: View) {
        Timber.d("onDetach")
        groupLifecycleManager?.onPause()
    }

    override fun onDestroy() {
        Timber.d("onDestroy")
        groupLifecycleManager?.onDestroy(activity)
    }

    override fun onSaveViewState(view: View, outState: Bundle) {
        Timber.d("onSaveViewState")
    }

    override fun onAttach(view: View) {
        super.onAttach(view)
        Timber.d("onAttach")
        groupLifecycleManager?.onResume()
    }
}

class NextLifeCycleController : BaseController() {
    override fun title(): String? = "Next LC"

    override fun layoutId(): Int = R.layout.next_lc_layout


}
