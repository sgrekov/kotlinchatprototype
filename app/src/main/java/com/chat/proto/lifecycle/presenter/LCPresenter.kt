package com.chat.proto.lifecycle.presenter

import com.airbnb.rxgroups.AutoResubscribe
import com.airbnb.rxgroups.GroupLifecycleManager
import com.chat.proto.lifecycle.view.ILCTestView
import rx.Observable
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import timber.log.Timber
import java.util.concurrent.TimeUnit

class LCPresenter(val view: ILCTestView, val groupLifecycleManager: GroupLifecycleManager?) {

    private val OBSERVABLE_TAG = "timer"
    private val OBSERVABLE_TAG2 = "timer2"
    private val OBSERVABLE_TAG3 = "timer3"

    @AutoResubscribe val observer: Observer<Long> = object : Observer<Long> {

        override fun onNext(value: Long) {
            Timber.d("third timer tick:${value}")
        }

        override fun onError(e: Throwable) {
            Timber.e(e)
        }

        override fun onCompleted() {
            Timber.d("third timer comleted")
            view.setText(view.getText().toString() + "\n third timer finish")
            view.setBtnText("startTimers")
        }

        fun resubscriptionTag(): Any {
            return OBSERVABLE_TAG3
        }

    }

    fun startTimers() {
        view.setBtnText("started")
        Observable.timer(10, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(groupLifecycleManager?.transform<Long>(OBSERVABLE_TAG))
                .subscribe({
                    zero ->
                    run {
                        Timber.d("first timer finish:20")
                        view.setText("first timer finish:20")
                    }
                }, {
                    err ->
                    run {
                        Timber.e(err)
                    }
                })
        Observable.timer(15, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(groupLifecycleManager?.transform<Long>(OBSERVABLE_TAG2))
                .subscribe({
                    zero ->
                    run {
                        Timber.d("second timer finish:15")
                        view.setText(view.getText().toString() + "\n second timer finish:15")
                    }
                }, {
                    err ->
                    run {
                        Timber.e(err)
                    }
                })
        Observable.interval(16, 1, TimeUnit.SECONDS)
                .takeWhile { sec -> sec < 20 }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(groupLifecycleManager?.transform<Long>(OBSERVABLE_TAG3))
                .subscribe(observer)
    }

}