package com.chat.proto.splash.view.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.chat.proto.ChatAppKt
import com.chat.proto.R
import com.chat.proto.core.utility.UserPreferences
import com.chat.proto.login.view.activity.LoginActivity
import com.chat.proto.main.view.activity.MainActivity
import javax.inject.Inject

class SplashActivity : AppCompatActivity() {

    @Inject
    lateinit var userPreferences: UserPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        ChatAppKt.graph.inject(this)

        if (userPreferences.isLogged()) {
            startActivity(Intent(this, MainActivity::class.java))
        } else {
            startActivity(Intent(this, LoginActivity::class.java))
        }
        finish()
    }
}
