package com.chat.proto.layout.view.controller

import android.support.v7.app.ActionBarDrawerToggle
import android.view.View
import com.chat.proto.R
import com.chat.proto.core.view.BaseController

/**
 * Created by serg on 10/01/2017.
 */
class LayoutController(val toggle: ActionBarDrawerToggle?) : BaseController() {

    constructor() : this(null) {
    }

    override fun title(): String? = "Layout tests"

    override fun layoutId(): Int = R.layout.layout_test

    override fun onAttach(view: View) {
        super.onAttach(view)
        val ab = actionBar()

        ab?.setDisplayHomeAsUpEnabled(false)
        toggle?.setDrawerIndicatorEnabled(false)
        ab?.setDisplayHomeAsUpEnabled(true)
        toggle?.setToolbarNavigationClickListener {
            router.popCurrentController()
            toggle.setDrawerIndicatorEnabled(true)
        }
    }


    override fun handleBack(): Boolean {
        toggle?.setDrawerIndicatorEnabled(true)
        return super.handleBack()
    }
}